#!/usr/bin/env sh

set -eu

exportVariable() {
  if [ "$#" -eq 1 ]; then
    exportedVariableName=$1
    lookupVariableName=$1
    defaultVariableValue=''
  elif [ "$#" -eq 2 ]; then
    exportedVariableName=$1
    lookupVariableName=$1
    defaultVariableValue=$2
  elif [ "$#" -eq 3 ]; then
    exportedVariableName=$1
    lookupVariableName=$2
    defaultVariableValue=$3
  else
    return
  fi
  eval "export $exportedVariableName=\${$lookupVariableName:-$defaultVariableValue}"
}

unsetVariables() {
  for argument in "$@"; do
    variables=$(awk -v regexp="$argument" '
      BEGIN {
        for(envVar in ENVIRON) {
          if(envVar ~ regexp) {
            print envVar
          }
        }
      }
    ')
    if [ -n "$variables" ]; then
      logMessage "Unset variables with regexp $argument"
      # shellcheck disable=SC2086
      unset $variables
    fi
  done
}

# shellcheck disable=SC1091
main() {
  . /usr/local/bin/support/log-events

  unsetVariables '.*_VERSION$'
  unsetVariables '^PHP_CAS.*'
  unsetVariables '^(PHPIZE_|DEV_|)DEPS$'
}

main
