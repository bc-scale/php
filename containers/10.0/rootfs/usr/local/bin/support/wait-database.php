<?php

// phpcs:disable PSR1.Files.SideEffects

declare(strict_types=1);

require_once __DIR__ . '/utils.php';

/**
 * @noinspection PhpUnused
 */
function checkDatabase(): int
{
    try {
        $mysqlConnection = connectDatabase();
    } catch (mysqli_sql_exception $exception) {
        $errorPrefix = 'Failed to connect to MySQL';
        logMessage($exception->getMessage(), $errorPrefix, $exception->getCode());
        return match ($exception->getCode()) {
            1045 => 1,
            default => -1,
        };
    }
    logMessage("Database is available $mysqlConnection->server_info", 'Notice');
    return 0;
}

/**
 * @noinspection PhpUnused
 */
function checkSession(): int
{
    $errorPrefix = 'Failed to start session';
    try {
        $status = session_start() && session_destroy();
    } catch (Error $error) {
        logMessage($error->getMessage(), $errorPrefix, $error->getCode());
        return -1;
    }
    if ($status === false) {
        logMessage('Can\'t check if session is able to start', $errorPrefix);
        return 1;
    }
    logMessage('Session storage is available', 'Notice');
    return 0;
}

function main(): int
{
    initSignals();
    $checkResult = -1;
    while ($checkResult === -1) {
        $checks = [
            'checkSession',
            'checkDatabase',
        ];
        $checkResult = array_reduce(
            $checks,
            static function ($acc, $check) {
                $result = $check();
                if ($acc > 0) {
                    return $acc;
                }
                if ($result === -1 || $acc === -1) {
                    return -1;
                }
                return 0;
            },
            0
        );
        sleep(1);
    }
    return $checkResult;
}

/** @SuppressWarnings(PHPMD.ExitExpression) */
exit(main());
